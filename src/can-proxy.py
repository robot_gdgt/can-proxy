"""
CAN interface to redis proxy

Subscribers can:
	Add or delete filter/mask (CANfilter queue)
		The same filter/mask combo can be request by more than one subsciber
	Send CAN packets (CANtx queue)
		automatic loopback to other subscribers

When an incoming CAN packet is received:
	JSON-encoded and placed in subscriber queue based on filter/mask (<FILE> queue)
	if CAN_CMD_HALT initiate shutdown
"""

import sys
import subprocess
from time import sleep
from os import path
import json
import logging
import logger
import can
import redis
from status_led import StatusLED
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

FILE = path.splitext(path.basename(__file__))[0]

logger.init(FILE, logging.INFO)

if __name__ == "__main__":

    # setup
    hbLED = StatusLED(LED_PIN, True)

    try:
        red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        raise Exception('Is the redis server running?')

    subprocess.run("sudo ip link set can0 down", shell=True)
    subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
    bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
    can_filters = [
        {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False, "req_by": {"sys",}},
    ]
    bus.set_filters(can_filters)

    hbLED.heartbeat()

    # loop
    try:

        while True:

            # Get CAN messages and add them to the Redis queues
            new_msg = bus.recv(0.0)
            if new_msg is not None:
                logging.debug(new_msg)
                msg = {
                    'can_id': new_msg.arbitration_id,
                    'is_remote_frame': new_msg.is_remote_frame,
                    'dlc': new_msg.dlc,
                    'data': [x for x in new_msg.data]
                }
                logging.info(f'CANrx dict: {msg}')
                for filter in can_filters:
                    if msg['can_id'] & filter['can_mask'] == filter['can_id']:
                        for req_by in filter['req_by']:
                            if req_by == 'sys': continue
                            msgj = json.dumps(msg)
                            logging.info(f'Send: {msgj} to: {req_by}')
                            red.rpush(req_by, msgj)
                if msg['can_id'] == CAN_CMD_HALT:
                    sleep(5)
                    subprocess.run("sudo halt", shell=True)
                    sys.exit()

            # Check the outgoing Redis queue for a CAN message request
            new_msg = red.lpop("CANtx")
            if new_msg is not None:
                new_msg = json.loads(new_msg)
                logging.info(f'CANtx: {new_msg}')
                assert ('can_id' in new_msg), "CAN ID missing"
                irf = new_msg['irf'] if 'irf' in new_msg else False
                dlc = new_msg['dlc'] if 'dlc' in new_msg else (len(new_msg['data']) if 'data' in new_msg else 0)
                data = new_msg['data'] if 'data' in new_msg else []
                msg = can.Message(
                    arbitration_id=new_msg['can_id'],
                    is_extended_id=False,
                    is_remote_frame=irf,
                    dlc=dlc,
                    data=data)
                bus.send(msg, timeout=0.2)
                for filter in can_filters:
                    if new_msg['can_id'] & filter['can_mask'] == filter['can_id']:
                        for req_by in filter['req_by']:
                            red.rpush(req_by, json.dumps(new_msg))

            # Process CAN filter action
            new_filter = red.lpop('CANfilter')
            if new_filter is not None:
                new_filter = json.loads(new_filter)
                logging.info(f'CANfilter: {new_filter}')
                if 'can_id' not in new_filter or 'can_mask' not in new_filter or 'action' not in new_filter or 'req_by' not in new_filter:
                    logging.debug('Missing one or more keys')
                elif new_filter['action'] == 'del':
                    # CAN filters can be set by more than one requestor. When deleting a filter it is removed
                    # only when there are no more requestors for that id/mask combination.
                    for i, filter in enumerate(can_filters):
                        if new_filter['can_id'] == 'all' or (new_filter['can_id'] == filter['can_id'] and new_filter['can_mask'] == filter['can_mask']):
                            filter['req_by'].discard(new_filter['req_by'])
                            if len(filter['req_by']) == 0:
                                can_filters.remove(i)
                                logging.debug(can_filters)
                                bus.set_filters(can_filters)
                            break
                else:
                    # CAN filters can be set by more than one requestor. When adding a filter it is added
                    # only when there are no other requestors for that id/mask combination. If there are
                    # the req_by of the existing id/mask combination is updated with the new req_by.
                    found = False
                    for i, filter in enumerate(can_filters):
                        if new_filter['can_id'] == filter['can_id'] and new_filter['can_mask'] == filter['can_mask']:
                            filter['req_by'].add(new_filter['req_by'])
                            found = True
                            break
                    if not found:
                        filter = {"can_id": new_filter['can_id'], "can_mask": new_filter['can_mask'], "extended": False, "req_by": {new_filter['req_by'],}}
                        can_filters.append(filter)
                        bus.set_filters(can_filters)
                    logging.debug(can_filters)

            # Blink the LED
            hbLED.tickle()

            sleep(0.05)

    except KeyboardInterrupt:
        logging.info('KeyboardInterrupt')
