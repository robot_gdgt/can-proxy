class StatusLED:

    def __init__(self, pin, on = False):
        pass

    def on(self):
        pass

    def off(self):
        pass

    def blink_once(self, blink_secs = 0.5):
        pass

    def heartbeat(self, on_secs = 0.05, off_secs = 1.95):
        pass

    def tickle(self):
        pass
