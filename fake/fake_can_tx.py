#!/usr/bin/python3

import sys
import subprocess
import time
import json
import redis
from can_ids import *
from var_dump import var_dump as vd
import json

if __name__ == "__main__":

    try:
        red = redis.Redis(unix_socket_path='/tmp/redis.sock')
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        sys.exit('Is the redis server running?')

    can_filters = [
        {"id": CAN_CMD_HALT, "mask": 0x7FF, "extended": False, "req_by": {"sys",}},
    ]

    # loop
    try:

        while True:
            now = time.time()

            # Get CAN messages and add them to the Redis queue
            msg = red.lpop("CANrx")
            if msg is not None:
                msg = json.loads(msg)
                print('CANrx', msg)
                for filter in can_filters:
    #                 vd(filter)
                    if msg['id'] & filter['mask'] == filter['id']:
                        for req_by in filter['req_by']:
                            if req_by == 'sys': continue
                            red.rpush(req_by, json.dumps(msg))
                if msg['id'] == CAN_CMD_HALT:
    #                 time.sleep(5)
    #                 subprocess.run("sudo halt", shell=True)
                    sys.exit()

            # Check the outgoing Redis queue for a CAN message request
            new_msg = red.lpop("CANtx")
            if new_msg is not None:
    #           var_dump(new_msg)
                new_msg = json.loads(new_msg)
                print('CANtx', new_msg)
                assert ('id' in new_msg), "CAN ID missing"
                irf = new_msg['irf'] if 'irf' in new_msg else False
                dlc = new_msg['dlc'] if 'dlc' in new_msg else (len(new_msg['data']) if 'data' in new_msg else 0)
                data = new_msg['data'] if 'data' in new_msg else []
                for filter in can_filters:
                    if new_msg['id'] & filter['mask'] == filter['id']:
                        for req_by in filter['req_by']:
                            red.rpush(req_by, json.dumps(new_msg))

            # Process CAN filter action
            new_filter = red.lpop('CANfilter')
            if new_filter is not None:
    #           var_dump(new_filter)
                new_filter = json.loads(new_filter)
                print('CANfilter', new_filter)
                if new_filter['action'] == 'del':
                    # CAN filters can be set by more than one requestor. When deleting a filter it is removed
                    # only when there are no more requestors for that id/mask combination.
                    for i, filter in enumerate(can_filters):
                        if new_filter['id'] == filter['id'] and new_filter['mask'] == filter['mask']:
                            filter['req_by'].discard(new_filter['req_by'])
                            if len(filter['req_by']) == 0:
                                can_filters.remove(i)
                                print(can_filters)
                            break
                else:
                    # CAN filters can be set by more than one requestor. When adding a filter it is added
                    # only when there are no other requestors for that id/mask combination. If there are
                    # the req_by of the existing id/mash combination is updated with the new req_by.
                    found = False
                    for i, filter in enumerate(can_filters):
                        if new_filter['id'] == filter['id'] and new_filter['mask'] == filter['mask']:
                            filter['req_by'].add(new_filter['req_by'])
                            found = True
                            break
                    if not found:
                        filter = {"id": new_filter['id'], "mask": new_filter['mask'], "extended": False, "req_by": {new_filter['req_by'],}}
                        can_filters.append(filter)
                    print(can_filters)

    except KeyboardInterrupt:
        pass
