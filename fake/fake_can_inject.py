#!/usr/bin/python3

import sys
import subprocess
import time
import json
import redis
from can_ids import *
import json
# from var_dump import var_dump as vd

if __name__ == "__main__":

    # setup

    try:
        red = redis.Redis(unix_socket_path='/tmp/redis.sock')
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        sys.exit('Is the redis server running?')

    # loop
    while True:
        id = input('ID: ')
        if id == 'q': quit()
        id = int(id, 16)
        if id == 0: quit()
        din = input('data: ')
        if din == 'q': quit()
        dlc = int(len(din) / 2)
        print(dlc)
        data = []
        for i in range(dlc):
            data.append(int(din[i * 2: i * 2 + 2], 16))
        print(data)

        new_msg = {'id': id, 'dlc': dlc, 'data': data}
#         print(new_msg)
        red.rpush('CANrx', json.dumps(new_msg))
