# Fake LED

class LED:
    def __init__(self, pin):
        print('   ', end='', flush=True)
        self.is_lit = False

    def on(self):
        print('\b\b\b*  ', end='', flush=True)
        self.is_lit = True

    def off(self):
        print('\b\b\b   ', end='', flush=True)
        self.is_lit = False
