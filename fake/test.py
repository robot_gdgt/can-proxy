#!/usr/bin/python3

import time
from status_led import StatusLED

if __name__ == "__main__":

    # setup
    hbLED = StatusLED(25, True)

    time.sleep(1)

    hbLED.heartbeat()

    # loop
    while True:

        # Blink the LED
        hbLED.tickle()

        time.sleep(0.01)
